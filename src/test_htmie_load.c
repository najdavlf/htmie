/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* snprintf support */

#include "htmie.h"
#include "test_htmie_utils.h"

#include <rsys/math.h>

#include <stdlib.h>
#include <string.h>


#define NEAREST HTMIE_FILTER_NEAREST
#define LINEAR HTMIE_FILTER_LINEAR

static INLINE double
rand_canonic(void)
{
  return rand() / (double)(RAND_MAX - 1);
}

static void
test_fetch(struct htmie* htmie)
{
  size_t nwlens;
  size_t i;
  const double* wlens;
  const double* absor;
  const double* scatt;
  const double* asym;
  double wlen;
  double xabs;
  double xsca;
  double u;
  double g;

  CHK(htmie);

  nwlens = htmie_get_wavelengths_count(htmie);
  CHK(nwlens > 1);

  wlens = htmie_get_wavelengths(htmie);
  scatt = htmie_get_xsections_scattering(htmie);
  absor = htmie_get_xsections_absorption(htmie);
  asym = htmie_get_asymmetry_parameter(htmie);

  /* Test clamp to border */
  CHK(absor[0] == htmie_fetch_xsection_absorption(htmie, wlens[0]-1, NEAREST));
  CHK(scatt[0] == htmie_fetch_xsection_scattering(htmie, wlens[0]-1, NEAREST));
  CHK(asym[0] == htmie_fetch_asymmetry_parameter(htmie, wlens[0]-1, NEAREST));
  CHK(absor[0] == htmie_fetch_xsection_absorption(htmie, wlens[0]-1, LINEAR));
  CHK(scatt[0] == htmie_fetch_xsection_scattering(htmie, wlens[0]-1, LINEAR));
  CHK(asym[0] == htmie_fetch_asymmetry_parameter(htmie, wlens[0]-1, LINEAR));

  i = nwlens - 1;
  CHK(absor[i] == htmie_fetch_xsection_absorption(htmie, wlens[i]+1, NEAREST));
  CHK(scatt[i] == htmie_fetch_xsection_scattering(htmie, wlens[i]+1, NEAREST));
  CHK(asym[i] == htmie_fetch_asymmetry_parameter(htmie, wlens[i]+1, NEAREST));
  CHK(absor[i] == htmie_fetch_xsection_absorption(htmie, wlens[i]+1, LINEAR));
  CHK(scatt[i] == htmie_fetch_xsection_scattering(htmie, wlens[i]+1, LINEAR));
  CHK(asym[i] == htmie_fetch_asymmetry_parameter(htmie, wlens[i]+1, LINEAR));


  FOR_EACH(i, 0, nwlens-1) {
    CHK(absor[i+0] == htmie_fetch_xsection_absorption(htmie, wlens[i+0], NEAREST));
    CHK(absor[i+1] == htmie_fetch_xsection_absorption(htmie, wlens[i+1], NEAREST));
    CHK(scatt[i+0] == htmie_fetch_xsection_scattering(htmie, wlens[i+0], NEAREST));
    CHK(scatt[i+1] == htmie_fetch_xsection_scattering(htmie, wlens[i+1], NEAREST));
    CHK(asym[i+1] == htmie_fetch_asymmetry_parameter(htmie, wlens[i+1], NEAREST));

    u = 0.25;
    wlen = wlens[i+0] + u * (wlens[i+1] - wlens[i+0]);
    xabs = absor[i+0] + u * (absor[i+1] - absor[i+0]);
    xsca = scatt[i+0] + u * (scatt[i+1] - scatt[i+0]);
    g = asym[i+0] + u *(asym[i+1] - asym[i+0]);
    CHK(absor[i+0] == htmie_fetch_xsection_absorption(htmie, wlen, NEAREST));
    CHK(scatt[i+0] == htmie_fetch_xsection_scattering(htmie, wlen, NEAREST));
    CHK(asym[i+0] == htmie_fetch_asymmetry_parameter(htmie, wlen, NEAREST));
    CHK(eq_eps(xabs, htmie_fetch_xsection_absorption(htmie, wlen, LINEAR), 1.e-6));
    CHK(eq_eps(xsca, htmie_fetch_xsection_scattering(htmie, wlen, LINEAR), 1.e-6));
    CHK(eq_eps(g, htmie_fetch_asymmetry_parameter(htmie, wlen, LINEAR), 1.e-6));

    u = 0.51;
    wlen = wlens[i+0] + u * (wlens[i+1] - wlens[i+0]);
    xabs = absor[i+0] + u * (absor[i+1] - absor[i+0]);
    xsca = scatt[i+0] + u * (scatt[i+1] - scatt[i+0]);
    g = asym[i+0] + u *(asym[i+1] - asym[i+0]);
    CHK(absor[i+1] == htmie_fetch_xsection_absorption(htmie, wlen, NEAREST));
    CHK(scatt[i+1] == htmie_fetch_xsection_scattering(htmie, wlen, NEAREST));
    CHK(asym[i+1] == htmie_fetch_asymmetry_parameter(htmie, wlen, NEAREST));
    CHK(eq_eps(xabs, htmie_fetch_xsection_absorption(htmie, wlen, LINEAR), 1.e-6));
    CHK(eq_eps(xsca, htmie_fetch_xsection_scattering(htmie, wlen, LINEAR), 1.e-6));
    CHK(eq_eps(g, htmie_fetch_asymmetry_parameter(htmie, wlen, LINEAR), 1.e-6));
  }
}

static void
test_bounds(struct htmie* htmie)
{
  double bounds[2];
  double band[2];
  double min_val = DBL_MAX;
  double max_val =-DBL_MAX;
  const double* wlens;
  const size_t ntests = 128;
  size_t ilow;
  size_t iupp;
  size_t nwlens;
  size_t i;
  size_t itest;
  CHK(htmie);

  wlens = htmie_get_wavelengths(htmie);
  nwlens = htmie_get_wavelengths_count(htmie);

  CHK(nwlens > 1);

  FOR_EACH(itest, 0, ntests) {
    do {
      ilow = (size_t)(rand_canonic() * (double)nwlens);
      iupp = (size_t)(rand_canonic() * (double)nwlens);
    } while(ilow == iupp);

    if(ilow > iupp) SWAP(size_t, ilow, iupp);
    band[0] = wlens[ilow];
    band[1] = wlens[iupp];

    #define TEST(Name, Filter) {                                               \
      htmie_compute_xsection_## Name ## _bounds(htmie, band, Filter, bounds);  \
      min_val = htmie_fetch_xsection_ ## Name(htmie, band[0], Filter);         \
      max_val = htmie_fetch_xsection_ ## Name(htmie, band[1], Filter);         \
      if(min_val > max_val) SWAP(double, min_val, max_val);                    \
      FOR_EACH(i, ilow+1, iupp) {                                              \
        double tmp = htmie_fetch_xsection_ ## Name(htmie, wlens[i], Filter);   \
        min_val = MMIN(min_val, tmp);                                          \
        max_val = MMAX(max_val, tmp);                                          \
      }                                                                        \
      CHK(eq_eps(bounds[0], min_val, 1.e-6));                                  \
      CHK(eq_eps(bounds[1], max_val, 1.e-6));                                  \
    } (void)0

    TEST(absorption, NEAREST);
    TEST(scattering, NEAREST);
    TEST(absorption, LINEAR);
    TEST(scattering, LINEAR);

    band[0] = wlens[ilow] + 0.25*(wlens[ilow+1] - wlens[ilow+0]);
    band[1] = wlens[iupp] - 0.25*(wlens[iupp+0] - wlens[iupp-1]);
    CHK(band[0] < band[1]);

    TEST(absorption, NEAREST);
    TEST(scattering, NEAREST);
    TEST(absorption, LINEAR);
    TEST(scattering, LINEAR);

    #undef TEST

    band[0] = band[1];

    htmie_compute_xsection_absorption_bounds(htmie, band, NEAREST, bounds);
    min_val = htmie_fetch_xsection_absorption(htmie, band[0], NEAREST);
    max_val = htmie_fetch_xsection_absorption(htmie, band[1], NEAREST);
    CHK(eq_eps(bounds[0], min_val, 1.e-6));
    CHK(eq_eps(bounds[1], max_val, 1.e-6));

    htmie_compute_xsection_absorption_bounds(htmie, band, LINEAR, bounds);
    min_val = htmie_fetch_xsection_absorption(htmie, band[0], LINEAR);
    max_val = htmie_fetch_xsection_absorption(htmie, band[1], LINEAR);
    CHK(eq_eps(bounds[0], min_val, 1.e-6));
    CHK(eq_eps(bounds[1], max_val, 1.e-6));

    htmie_compute_xsection_scattering_bounds(htmie, band, NEAREST, bounds);
    min_val = htmie_fetch_xsection_scattering(htmie, band[0], NEAREST);
    max_val = htmie_fetch_xsection_scattering(htmie, band[1], NEAREST);
    CHK(eq_eps(bounds[0], min_val, 1.e-6));
    CHK(eq_eps(bounds[1], max_val, 1.e-6));

    htmie_compute_xsection_scattering_bounds(htmie, band, LINEAR, bounds);
    min_val = htmie_fetch_xsection_scattering(htmie, band[0], LINEAR);
    max_val = htmie_fetch_xsection_scattering(htmie, band[1], LINEAR);
    CHK(eq_eps(bounds[0], min_val, 1.e-6));
    CHK(eq_eps(bounds[1], max_val, 1.e-6));
  }
}

static void
test_avg(struct htmie* htmie)
{
  double band[2];
  const double* wlens;
  const size_t ntests = 128;
  size_t itest;
  size_t nwlens;
  size_t ilow;
  size_t iupp;
  size_t i;
  CHK(htmie);

  wlens = htmie_get_wavelengths(htmie);
  nwlens = htmie_get_wavelengths_count(htmie);

  CHK(nwlens > 1);

  FOR_EACH(itest, 0, ntests) {
    do {
      ilow = (size_t)(rand_canonic() * (double)nwlens);
      iupp = (size_t)(rand_canonic() * (double)nwlens);
    } while(ilow == iupp);

    if(ilow > iupp) SWAP(size_t, ilow, iupp);
    band[0] = wlens[ilow];
    band[1] = wlens[iupp];

    #define TEST(Name, Filter) {                                               \
      double avg = 0;                                                          \
      double sum = 0;                                                          \
      double data = 0;                                                         \
      double prev_wlen = band[0];                                              \
      double prev_data = htmie_fetch_ ## Name(htmie, band[0], Filter);\
      FOR_EACH(i, ilow+1, iupp) {                                              \
        data = htmie_fetch_ ## Name(htmie, wlens[i], Filter);         \
        sum += 0.5*(prev_data + data) * (wlens[i] - prev_wlen);                \
        prev_wlen = wlens[i];                                                  \
        prev_data = data;                                                      \
      }                                                                        \
      data = htmie_fetch_ ## Name(htmie, band[1], Filter);            \
      sum += 0.5*(prev_data + data) * (band[1] - prev_wlen);                   \
      sum /= (band[1] - band[0]);                                              \
      avg = htmie_compute_## Name ## _average(htmie, band, Filter);   \
      CHK(eq_eps(avg, sum, 1.e-6));                                            \
    } (void)0

    TEST(xsection_absorption, NEAREST);
    TEST(xsection_scattering, NEAREST);
    TEST(asymmetry_parameter, NEAREST);
    TEST(xsection_absorption, LINEAR);
    TEST(xsection_scattering, LINEAR);
    TEST(asymmetry_parameter, LINEAR);

    band[0] = wlens[ilow] + 0.25*(wlens[ilow+1] - wlens[ilow+0]);
    band[1] = wlens[iupp] - 0.25*(wlens[iupp+0] - wlens[iupp-1]);
    CHK(band[0] < band[1]);

    TEST(xsection_absorption, NEAREST);
    TEST(xsection_scattering, NEAREST);
    TEST(asymmetry_parameter, NEAREST);
    TEST(xsection_absorption, LINEAR);
    TEST(xsection_scattering, LINEAR);
    TEST(asymmetry_parameter, LINEAR);

    #undef TEST
  }
}

int
main(int argc, char** argv)
{
  char buf[128];
  struct mem_allocator allocator;
  char* filename = NULL;
  char* path = NULL;
  char* base = NULL;
  char* p = NULL;
  FILE* fp = NULL;
  struct htmie* htmie = NULL;
  double rmod, smod;
  size_t i;

  if(argc < 3) {
    fprintf(stderr, "Usage: %s <netcdf> <ref-data-path>\n", argv[0]);
    return -1;
  }

  filename = argv[1];
  path = argv[2];

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  CHK(htmie_create(NULL, &allocator, 1, &htmie) == RES_OK);

  CHK(htmie_load(NULL, NULL) == RES_BAD_ARG);
  CHK(htmie_load(htmie, NULL) == RES_BAD_ARG);
  CHK(htmie_load(NULL, filename) == RES_BAD_ARG);
  CHK(htmie_load(htmie, filename) == RES_OK);

  p = strrchr(filename, '/');
  if(p) base = p+1;
  p = strrchr(base, '.');
  if(p) *p = '\0';

  /* Check the wavelengths list */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_lambda", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  FOR_EACH(i, 0, htmie_get_wavelengths_count(htmie)) {
    double lambda;
    CHK(fscanf(fp, "%lg", &lambda) == 1);
    CHK(eq_eps(lambda, htmie_get_wavelengths(htmie)[i], 1.e-6));
  }
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);

  /* Read rmod */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_rmod", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  CHK(fscanf(fp, "%lg", &rmod) == 1);
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);

  /* Read smod */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_smod", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  CHK(fscanf(fp, "%lg", &smod) == 1);
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);

  /* Check absorption cross sections */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_macs", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  FOR_EACH(i, 0, htmie_get_wavelengths_count(htmie)) {
    const double Cabs = htmie_get_xsections_absorption(htmie)[i];
    double macs;
    CHK(fscanf(fp, "%lg", &macs) == 1);
    CHK(eq_eps(macs, Cabs, 1.e-6));
  }
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);

  /* Check scattering cross sections */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_mscs", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  FOR_EACH(i, 0, htmie_get_wavelengths_count(htmie)) {
    const double Csca = htmie_get_xsections_scattering(htmie)[i];
    double mscs;
    CHK(fscanf(fp, "%lg", &mscs) == 1);
    CHK(eq_eps(mscs, Csca, 1.e-6));
  }
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);

  /* Check scattering asymmetry parameter */
  CHK((size_t)snprintf(buf, sizeof(buf), "%s/%s_g", path, base)<sizeof(buf));
  CHK(fp = fopen(buf, "r"));
  FOR_EACH(i, 0, htmie_get_wavelengths_count(htmie)) {
    double g;
    CHK(fscanf(fp, "%lg", &g) == 1);
    CHK(eq_eps(g, htmie_get_asymmetry_parameter(htmie)[i], 1.e-6));
  }
  CHK(fscanf(fp, "%*g") == EOF);
  CHK(fclose(fp) == 0);


  test_fetch(htmie);
  test_bounds(htmie);
  test_avg(htmie);

  CHK(htmie_ref_put(htmie) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

