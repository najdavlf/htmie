/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htmie.h"

#include <rsys/algorithm.h>
#include <rsys/dynamic_array_double.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

#include <netcdf.h>
#include <stdlib.h>

#define INVALID_NC_ID -1

#define NC(Func) {                                                             \
    const int err__ = nc_ ## Func;                                             \
    if(err__ != NC_NOERR) {                                                    \
      log_err(htmie, "error:%i:%s\n", __LINE__, ncerr_to_str(err__));          \
      abort();                                                                 \
    }                                                                          \
  } (void)0

/* Context used in the sum_xsections functor */
struct sum_context {
  double prev_data;
  double prev_wavelength;
  double sum;
  int first_entry;
};
static const struct sum_context SUM_CONTEXT_NULL =
  {DBL_MAX, DBL_MAX, 0, 1};

struct htmie {
  struct darray_double wavelengths; /* In nano-meters */
  struct darray_double xsections_absorption; /* In m^2.kg^-1 */
  struct darray_double xsections_scattering; /* In m^2.kg^-1 */
  struct darray_double g; /* Asymetric parameter */

  int verbose; /* Verbosity level */
  struct logger* logger;
  struct mem_allocator* allocator;
  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
log_msg
  (const struct htmie* htmie,
   const enum log_type stream,
   const char* msg,
   va_list vargs)
{
  ASSERT(htmie && msg);
  if(htmie->verbose) {
    res_T res; (void)res;
    res = logger_vprint(htmie->logger, stream, msg, vargs);
    ASSERT(res == RES_OK);
  }
}

static INLINE void
log_err(const struct htmie* htmie, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(htmie && msg);
  va_start(vargs_list, msg);
  log_msg(htmie, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

static INLINE void
log_warn(const struct htmie* htmie, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(htmie && msg);
  va_start(vargs_list, msg);
  log_msg(htmie, LOG_WARNING, msg, vargs_list);
  va_end(vargs_list);
}

static INLINE int
cmp_dbl(const void* a, const void* b)
{
  const double d0 = *((const double*)a);
  const double d1 = *((const double*)b);
  return d0 < d1 ? -1 : (d0 > d1 ? 1 : 0);
}

static INLINE const char*
ncerr_to_str(const int ncerror)
{
  const char* str = "NC_ERR_<UNKNOWN>";
  switch(ncerror) {
    case NC_EBADGRPID: str = "NC_EBADGRPID"; break;
    case NC_EBADID: str = "NC_EBADID"; break;
    case NC_EBADNAME: str = "NC_EBADNAME"; break;
    case NC_ECHAR: str = "NC_ECHAR"; break;
    case NC_EDIMMETA: str = "NC_EDIMMETA"; break;
    case NC_EHDFERR: str = "NC_EHDFERR"; break;
    case NC_ENOMEM: str = "NC_ENOMEM"; break;
    case NC_ENOTATT: str = "NC_ENOTATT"; break;
    case NC_ENOTVAR: str = "NC_ENOTVAR"; break;
    case NC_ERANGE: str = "NC_ERANGE"; break;
    case NC_NOERR: str = "NC_NOERR"; break;
  }
  return str;
}

static INLINE const char*
nctype_to_str(const nc_type type)
{
  const char* str = "NC_TYPE_<UNKNOWN>";
  switch(type) {
    case NC_NAT: str = "NC_NAT"; break;
    case NC_BYTE: str = "NC_BYTE"; break;
    case NC_CHAR: str = "NC_CHAR"; break;
    case NC_SHORT: str = "NC_SHORT"; break;
    case NC_LONG: str = "NC_LONG"; break;
    case NC_FLOAT: str = "NC_FLOAT"; break;
    case NC_DOUBLE: str = "NC_DOUBLE"; break;
    case NC_UBYTE: str = "NC_UBYTE"; break;
    case NC_USHORT: str = "NC_USHORT"; break;
    case NC_UINT: str = "NC_UINT"; break;
    case NC_INT64: str = "NC_INT64"; break;
    case NC_UINT64: str = "NC_UINT64"; break;
    case NC_STRING: str = "NC_STRING"; break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return str;
}

static INLINE size_t
sizeof_nctype(const nc_type type)
{
  size_t sz;
  switch(type) {
    case NC_BYTE:
    case NC_CHAR:
    case NC_UBYTE:
      sz = 1; break;
    case NC_SHORT:
    case NC_USHORT:
      sz = 2; break;
    case NC_FLOAT:
    case NC_INT:
    case NC_UINT:
      sz = 4; break;
    case NC_DOUBLE:
    case NC_INT64:
    case NC_UINT64:
      sz = 8; break;
    default: FATAL("Unreachable cde.\n"); break;
  }
  return sz;
}

static res_T
load_wavelengths(struct htmie* htmie, const int nc, const double range[2])
{
  size_t len;
  size_t start;
  size_t i;
  int id;
  int ndims;
  int dimid;
  int err= NC_NOERR;
  int type;
  res_T res = RES_OK;
  ASSERT(htmie && nc != INVALID_NC_ID && range && range[0] <= range[1]);

  err = nc_inq_varid(nc, "lambda", &id);
  if(err != NC_NOERR) {
    log_err(htmie, "Could not inquire the 'lambda' variable -- %s\n",
      ncerr_to_str(err));
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_varndims(nc, id, &ndims));
  if(ndims != 1) {
    log_err(htmie, "The dimension of the 'lambda' variable must be 1.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_vardimid(nc, id, &dimid));
  NC(inq_dimlen(nc, dimid, &len));
  NC(inq_vartype(nc, id, &type));

  if(type != NC_DOUBLE && type != NC_FLOAT) {
    log_err(htmie,
      "The type of the 'lambda' variable cannot be %s. "
      "Expecting floating point data.\n",
      nctype_to_str(type));
    res = RES_BAD_ARG;
    goto error;
  }

  res = darray_double_resize(&htmie->wavelengths, len);
  if(res != RES_OK) {
    log_err(htmie, "Could not allocate the list of wavelengths.\n");
    goto error;
  }

  start = 0;
  NC(get_vara_double(nc, id, &start, &len,
    darray_double_data_get(&htmie->wavelengths)));

  /* Check data validity */
  FOR_EACH(i, 0, len) {
    if(darray_double_cdata_get(&htmie->wavelengths)[i] < range[0]
    || darray_double_cdata_get(&htmie->wavelengths)[i] > range[1]) {
      log_err(htmie, "Invalid wavelength %g. It must be in [%g, %g]\n",
        darray_double_cdata_get(&htmie->wavelengths)[i],
        range[0], range[1]);
      res = RES_BAD_ARG;
      goto error;
    }
  }

  FOR_EACH(i, 1, len) {
    if(darray_double_cdata_get(&htmie->wavelengths)[i-1]
     > darray_double_cdata_get(&htmie->wavelengths)[i-0]) {
      break;
    }
  }

  if(i < len) { /* Wavelengths are not sorted */
    log_warn(htmie, "Unordered wavelengths.\n");
    qsort
      (darray_double_data_get(&htmie->wavelengths),
       darray_double_size_get(&htmie->wavelengths),
       sizeof(double),
       cmp_dbl);
  }

exit:
  return res;
error:
  darray_double_clear(&htmie->wavelengths);
  goto exit;
}

static res_T
load_distrib_x_lambda_array
  (struct htmie* htmie,
   const int nc,
   const char* var_name,
   const double range[2], /* Validity range */
   struct darray_double* values)
{
  size_t start[2];
  size_t end[2];
  size_t len;
  size_t i;
  int dimids[2];
  int id;
  int ndims;
  int err;
  int type;
  res_T res = RES_OK;
  ASSERT(htmie && nc != INVALID_NC_ID && var_name && values && range);
  ASSERT(range[0] <= range[1]);

  err = nc_inq_varid(nc, var_name, &id);
  if(err != NC_NOERR) {
    log_err(htmie, "Could not inquire the '%s' variable -- %s\n",
      var_name, ncerr_to_str(err));
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_varndims(nc, id, &ndims));
  if(ndims != 2) {
    log_err(htmie,
      "The dimension of the '%s' variable must be 2.\n", var_name);
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_vardimid(nc, id, dimids));
  NC(inq_vartype(nc, id, &type));

  NC(inq_dimlen(nc, dimids[0], &len));
  if(len != 1) {
    log_err(htmie,
      "Only 1 distribution is currently supported while the #distributions of "
      "the '%s' variable is %lu\n", var_name, (unsigned long)len);
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_dimlen(nc, dimids[1], &len));
  if(len != darray_double_size_get(&htmie->wavelengths)) {
    log_err(htmie,
      "Inconsistent wavelengths count. The number of defined wavelengths is %lu "
      "while the per distribution length of the '%s' variable is %lu.\n",
      darray_double_size_get(&htmie->wavelengths), var_name, len);
    res = RES_BAD_ARG;
    goto error;
  }

  if(type != NC_DOUBLE && type != NC_FLOAT) {
    log_err(htmie,
      "The type of the '%s' variable cannot be %s. "
      "Expecting floating point data.\n",
      var_name, nctype_to_str(type));
    res = RES_BAD_ARG;
    goto error;
  }

  res = darray_double_resize(values, len);
  if(res != RES_OK) {
    log_err(htmie,
      "Could not allocate the memory space to load the data of the '%s' variable.\n",
      var_name);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Read the raw data sections */
  start[0] = 0, start[1] = 0;
  end[0] = 1, end[1] = len;
  NC(get_vara_double(nc, id, start, end, darray_double_data_get(values)));

  FOR_EACH(i, 0, darray_double_size_get(values)) {
    const double* d = darray_double_cdata_get(values);
    if(d[i] < range[0] || d[i] > range[1]) {
      log_err(htmie,
        "Invalid value for the %lu^th data of the '%s' variable : %g. "
        "The data must be in [%g, %g]\n",
        (unsigned long)i, var_name, d[i], range[0], range[1]);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  darray_double_clear(values);
  goto exit;
}

static FINLINE double
interpolate_data
  (const struct htmie* htmie,
   const double wavelength, /* Input wavelength */
   const enum htmie_filter_type type, /* Interpolation type */
   const size_t idata, /* Id of the upper bound data wrt `wavelength' */
   const double* data) /* Data to interpolate */
{
  const double* wlens;
  double a, b, u;
  double val;
  ASSERT(data);

  wlens = htmie_get_wavelengths(htmie);

  ASSERT(idata < htmie_get_wavelengths_count(htmie));
  ASSERT(wlens[idata-1] < wavelength && wavelength <= wlens[idata-0]);

  a = data[idata-1];
  b = data[idata-0];
  u = (wavelength - wlens[idata-1]) / (wlens[idata] - wlens[idata-1]);
  ASSERT(eq_eps(u, 1, 1.e-6) || u < 1);
  ASSERT(eq_eps(u, 0, 1.e-6) || u > 0);

  switch(type) {
    case HTMIE_FILTER_NEAREST:
      val = u < 0.5 ? a : b;
      break;
    case HTMIE_FILTER_LINEAR:
      u = CLAMP(u, 0, 1); /* Handle numerical inaccuracy */
      val = u*b + (1-u)*a;
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return val;
}

static FINLINE double
fetch_data
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type,
   const double* data) /* Data to interpolate */
{
  size_t nwlens;
  const double* wlens;
  const double* wlen_upp;
  double val;
  ASSERT(htmie && (unsigned)type < HTMIE_FILTER_TYPES_COUNT__ && data);

  wlens = htmie_get_wavelengths(htmie);
  nwlens = htmie_get_wavelengths_count(htmie);
  ASSERT(nwlens);

  wlen_upp = search_lower_bound
    (&wavelength, wlens, nwlens, sizeof(double), cmp_dbl);

  if(!wlen_upp) { /* Clamp to upper */
    val = data[nwlens-1];
  } else if(wlen_upp == wlens) { /* Clamp to lower */
    val = data[0];
  } else {
    const size_t i = (size_t)(wlen_upp - wlens);
    val = interpolate_data(htmie, wavelength, type, i, data);
  }
  return val;
}

static FINLINE void
min_max(const double wavelength, const double data, void* ctx)
{
  double* bounds = ctx;
  ASSERT(ctx);
  (void)wavelength;
  bounds[0] = MMIN(bounds[0], data);
  bounds[1] = MMAX(bounds[1], data);
}

static FINLINE void
sum(const double wavelength, const double data, void* context)
{
  struct sum_context* ctx = context;
  ASSERT(context);

  if(ctx->first_entry) {
    ASSERT(ctx->sum == 0);
    ctx->first_entry = 0;
  } else {
    ASSERT(wavelength > ctx->prev_wavelength);
    ctx->sum +=
      0.5 * (ctx->prev_data + data)*(wavelength - ctx->prev_wavelength);
  }
  ctx->prev_wavelength = wavelength;
  ctx->prev_data = data;
}

static INLINE void
for_each_wavelength_in_spectral_band
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of the spectral band in nanometer */
   const enum htmie_filter_type type,
   const double* data, /* Input data of the spectral band*/
   void (*op)(const double wavelength, const double data, void* ctx),
   void* context) /* Sent as the last argument of the 'op' functor */
{
  const double* wlens;
  const double* wlen_low;
  const double* wlen_upp;
  double data_low;
  double data_upp;
  size_t ilow;
  size_t iupp;
  size_t nwlens;
  size_t i;
  ASSERT(htmie && band[0] <= band[1]);

  wlens = htmie_get_wavelengths(htmie);
  nwlens = htmie_get_wavelengths_count(htmie);
  ASSERT(nwlens);

  wlen_low = search_lower_bound
    (&band[0], wlens, nwlens, sizeof(double), cmp_dbl);
  wlen_upp = search_lower_bound
    (&band[1], wlens, nwlens, sizeof(double), cmp_dbl);

  if(!wlen_low) {
    ilow = nwlens;
    data_low = data[nwlens - 1];
  } else if(wlen_low == wlens) {
    ilow = 1;
    data_low = data[0];
  } else {
    ilow = (size_t)(wlen_low - wlens);
    data_low = interpolate_data(htmie, band[0], type, ilow, data);
  }

  if(!wlen_upp) {
    iupp = nwlens;
    data_upp = data[nwlens - 1];
  } else if(wlen_upp == wlens) {
    iupp = 1;
    data_upp = data[0];
  } else {
    iupp = (size_t)(wlen_upp - wlens);
    data_upp = interpolate_data(htmie, band[1], type, iupp, data);
  }

  if(wlens[ilow] == band[0]) ++ilow;

  op(band[0], data_low, context);
  FOR_EACH(i, ilow, iupp) {
    op(wlens[i], data[i], context);
  }
  if(band[0] != band[1])
    op(band[1], data_upp, context);
}

static void
release_htmie(ref_T* ref)
{
  struct htmie* htmie;
  ASSERT(ref);
  htmie = CONTAINER_OF(ref, struct htmie, ref);
  darray_double_release(&htmie->wavelengths);
  darray_double_release(&htmie->xsections_absorption);
  darray_double_release(&htmie->xsections_scattering);
  darray_double_release(&htmie->g);
  MEM_RM(htmie->allocator, htmie);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
htmie_create
  (struct logger* log,
   struct mem_allocator* mem_allocator,
   const int verbose,
   struct htmie** out_htmie)
{
  struct htmie* htmie = NULL;
  struct mem_allocator* allocator = NULL;
  struct logger* logger = NULL;
  res_T res = RES_OK;

  if(!out_htmie) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  logger = log ? log : LOGGER_DEFAULT;

  htmie = MEM_CALLOC(allocator, 1, sizeof(struct htmie));
  if(!htmie) {
    if(verbose) {
      logger_print(logger, LOG_ERROR,
        "%s: could not allocate the HTMIE handler.\n", FUNC_NAME);
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&htmie->ref);
  htmie->allocator = allocator;
  htmie->logger = logger;
  htmie->verbose = verbose;
  darray_double_init(htmie->allocator, &htmie->wavelengths);
  darray_double_init(htmie->allocator, &htmie->xsections_absorption);
  darray_double_init(htmie->allocator, &htmie->xsections_scattering);
  darray_double_init(htmie->allocator, &htmie->g);

exit:
  if(out_htmie) *out_htmie = htmie;
  return res;
error:
  if(htmie) {
    HTMIE(ref_put(htmie));
    htmie = NULL;
  }
  goto exit;
}

res_T
htmie_ref_get(struct htmie* htmie)
{
  if(!htmie) return RES_BAD_ARG;
  ref_get(&htmie->ref);
  return RES_OK;
}

res_T
htmie_ref_put(struct htmie* htmie)
{
  if(!htmie) return RES_BAD_ARG;
  ref_put(&htmie->ref, release_htmie);
  return RES_OK;
}

res_T
htmie_load(struct htmie* htmie, const char* path)
{
  int err_nc = NC_NOERR;
  int nc = INVALID_NC_ID;
  double range[2];
  res_T res = RES_OK;

  if(!htmie || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  err_nc = nc_open(path, NC_NOWRITE, &nc);
  if(err_nc != NC_NOERR) {
    log_err(htmie, "error opening file `%s' -- %s.\n", path, ncerr_to_str(err_nc));
    res = RES_BAD_ARG;
    goto error;
  }

  /* Define the validity range for the loaded data */
  range[0] = DBL_EPSILON;
  range[1] = DBL_MAX;

  #define CALL(Func) { res = Func; if(res != RES_OK) goto error; } (void)0
  CALL(load_wavelengths(htmie, nc, range));
  CALL(load_distrib_x_lambda_array
    (htmie, nc, "macs", range, &htmie->xsections_absorption));
  CALL(load_distrib_x_lambda_array
    (htmie, nc, "mscs", range, &htmie->xsections_scattering));
  CALL(load_distrib_x_lambda_array(htmie, nc, "g", range, &htmie->g));
  #undef CALL

exit:
  if(nc != INVALID_NC_ID) NC(close(nc));
  return res;
error:
  goto exit;
}

const double*
htmie_get_wavelengths(const struct htmie* htmie)
{
  ASSERT(htmie);
  return darray_double_cdata_get(&htmie->wavelengths);
}

const double*
htmie_get_xsections_absorption(const struct htmie* htmie)
{
  ASSERT(htmie);
  return darray_double_cdata_get(&htmie->xsections_absorption);
}

const double*
htmie_get_xsections_scattering(const struct htmie* htmie)
{
  ASSERT(htmie);
  return darray_double_cdata_get(&htmie->xsections_scattering);
}

const double*
htmie_get_asymmetry_parameter(const struct htmie* htmie)
{
  ASSERT(htmie);
  return darray_double_cdata_get(&htmie->g);
}

size_t
htmie_get_wavelengths_count(const struct htmie* htmie)
{
  ASSERT(htmie);
  return darray_double_size_get(&htmie->wavelengths);
}

double
htmie_fetch_xsection_absorption
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type)
{
  ASSERT(htmie);
  return fetch_data
    (htmie, wavelength, type, htmie_get_xsections_absorption(htmie));
}

double
htmie_fetch_xsection_scattering
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type)
{
  ASSERT(htmie);
  return fetch_data
    (htmie, wavelength, type, htmie_get_xsections_scattering(htmie));
}

double
htmie_fetch_asymmetry_parameter
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type)
{
  ASSERT(htmie);
  return fetch_data
    (htmie, wavelength, type, htmie_get_asymmetry_parameter(htmie));
}

void
htmie_compute_xsection_absorption_bounds
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of the spectral band in nanometer */
   const enum htmie_filter_type type,
   double bounds[2]) /* Min and Max scattering cross sections in m^2.kg^-1 */
{
  const double* xsections;
  ASSERT(htmie);
  bounds[0] = DBL_MAX;
  bounds[1] =-DBL_MAX;
  xsections = darray_double_cdata_get(&htmie->xsections_absorption);
  for_each_wavelength_in_spectral_band
    (htmie, band, type, xsections, min_max, bounds);
}

void
htmie_compute_xsection_scattering_bounds
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of the spectral band in nanometer */
   const enum htmie_filter_type type,
   double bounds[2]) /* Min and Max scattering cross sections in m^2.kg^-1 */
{
  const double* xsections;
  ASSERT(htmie);
  bounds[0] = DBL_MAX;
  bounds[1] =-DBL_MAX;
  xsections = darray_double_cdata_get(&htmie->xsections_scattering);
  for_each_wavelength_in_spectral_band
    (htmie, band, type, xsections, min_max, bounds);
}

double
htmie_compute_xsection_absorption_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type)
{
  const double* xsections;
  struct sum_context ctx = SUM_CONTEXT_NULL;
  ASSERT(htmie && band && band[0] < band[1]);
  xsections = darray_double_cdata_get(&htmie->xsections_absorption);
  for_each_wavelength_in_spectral_band(htmie, band, type, xsections, sum, &ctx);
  return ctx.sum / (band[1] - band[0]);
}

double
htmie_compute_xsection_scattering_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type)
{
  const double* xsections;
  struct sum_context ctx = SUM_CONTEXT_NULL;
  ASSERT(htmie && band && band[0] < band[1]);
  xsections = darray_double_cdata_get(&htmie->xsections_scattering);
  for_each_wavelength_in_spectral_band(htmie, band, type, xsections, sum, &ctx);
  return ctx.sum / (band[1] - band[0]);
}

double
htmie_compute_asymmetry_parameter_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type)
{
  const double* g;
  struct sum_context ctx = SUM_CONTEXT_NULL;
  ASSERT(htmie && band && band[0] < band[1]);
  g = darray_double_cdata_get(&htmie->g);
  for_each_wavelength_in_spectral_band(htmie, band, type, g, sum, &ctx);
  return ctx.sum / (band[1] - band[0]);
}

