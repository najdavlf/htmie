/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTMIE_H
#define HTMIE_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(HTMIE_SHARED_BUILD) /* Build shared library */
  #define HTMIE_API extern EXPORT_SYM
#elif defined(HTMIE_STATIC) /* Use/build static library */
  #define HTMIE_API extern LOCAL_SYM
#else /* Use shared library */
  #define HTMIE_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the htmie function `Func'
 * returns an error. One should use this macro on htmie function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define HTMIE(Func) ASSERT(htmie_ ## Func == RES_OK)
#else
  #define HTMIE(Func) htmie_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

enum htmie_filter_type {
  HTMIE_FILTER_LINEAR,
  HTMIE_FILTER_NEAREST,
  HTMIE_FILTER_TYPES_COUNT__
};

/* Forward declaration of opaque data types */
struct htmie;

BEGIN_DECLS

/*******************************************************************************
 * HTMIE API
 ******************************************************************************/
HTMIE_API res_T
htmie_create
  (struct logger* logger, /* May be NULL <=> Use default logger */
   struct mem_allocator* allocator, /* May be NULL <=> Use default allocator */
   const int verbose, /* Verbosity level */
   struct htmie** htmie);

HTMIE_API res_T
htmie_ref_get
  (struct htmie* htmie);

HTMIE_API res_T
htmie_ref_put
  (struct htmie* htmie);

HTMIE_API res_T
htmie_load
  (struct htmie *htmie,
   const char* path);

/* In nano-meter */
HTMIE_API const double*
htmie_get_wavelengths
  (const struct htmie* htmie);

/* In m^2.kg^-1 */
HTMIE_API const double*
htmie_get_xsections_absorption
  (const struct htmie* htmie);

/* In m^2.kg^-1 */
HTMIE_API const double*
htmie_get_xsections_scattering
  (const struct htmie* htmie);

HTMIE_API const double*
htmie_get_asymmetry_parameter
  (const struct htmie* htmie);

HTMIE_API size_t
htmie_get_wavelengths_count
  (const struct htmie* htmie);

/* In m^2.kg^-1 */
HTMIE_API double
htmie_fetch_xsection_absorption
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type);

/* In m^2.kg^-1 */
HTMIE_API double
htmie_fetch_xsection_scattering
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type);

HTMIE_API double
htmie_fetch_asymmetry_parameter
  (const struct htmie* htmie,
   const double wavelength,
   const enum htmie_filter_type type);

/* Compute the maximum and the minimum value of the absorption cross section in
 * a given spectral interval */
HTMIE_API void
htmie_compute_xsection_absorption_bounds
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of the spectral band in nanometer */
   const enum htmie_filter_type type,
   double bounds[2]); /* Min and Max scattering cross sections in m^2.kg^-1 */

/* Compute the maximum and the minimum value of the scattering cross section in
 * a given spectral interval */
HTMIE_API void
htmie_compute_xsection_scattering_bounds
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of the spectral band in nanometer */
   const enum htmie_filter_type type,
   double bounds[2]); /* Min and Max scattering cross sections in m^2.kg^-1 */

/* Compute the average absorption cross section in a given spectral interval */
HTMIE_API double
htmie_compute_xsection_absorption_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type);

/* Compute the average scattering cross section in a given spectral interval */
HTMIE_API double
htmie_compute_xsection_scattering_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type);

/* Compute the average asymmetry parameter in a given spectral interval */
HTMIE_API double
htmie_compute_asymmetry_parameter_average
  (const struct htmie* htmie,
   const double band[2], /* Boundaries of of the spectral band in nanometer */
   const enum htmie_filter_type type);

END_DECLS

#endif /* HTMIE_H */

