#!/bin/bash

# Copyright (C) 2018 |Meso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */
set -e
set -o pipefail

if [ $# -lt 2 ]; then
  echo "Usage: $0 VAR-NAME MIE-NETCDF "
  exit 0
fi

if [ ! -f $2 ]; then
  echo "\"$2\" is not a valid file."
  exit 0
fi

name=$(basename $2)
name=${name%.*}

ncdump -v $1 $2 \
  | sed -n "/^ *$1 *=/,\$p" \
  | sed "s/^ *$1 *= *//g" \
  | sed 's/[;} ]//g' \
  | sed 's/,/\n/g' \
  | sed '/^ *$/d' > ${name}_${1}

